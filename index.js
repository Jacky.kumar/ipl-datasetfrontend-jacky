fetch('data.json').then(data => data.json()).then(data => visualize(data));

function visualize(data) {

    let ecnomicaBowlersName = data["10EconomicalBowlers2015"].map(topBowler => Object.keys(topBowler));
    let ecnomicaBowlersEconomy = data["10EconomicalBowlers2015"].map(topBowler => Object.values(topBowler));

    const teamWonMatchYear = (matchesWonTeamByYear) => {
        return Object.keys(matchesWonTeamByYear).map(team => ({
            name: team,
            data: Object.values(matchesWonTeamByYear[team]),
            showInLegend: false,
        }))
    }


    Highcharts.chart('noOfmatchesYear', {
        chart: {
            type: 'bar'
        },
        title: {
            text: "Matches Per Year"
        },

        xAxis: {

            categories: Object.keys(data["matchesPerYear"]),
            title: {
                text: 'Years'
            }
        },
        yAxis: {
            min: 40,
            title: {
                text: 'No of matches',

            },

        },

        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }

        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Matches',

            data: Object.values(data["matchesPerYear"]),
            showInLegend: false,
        }],
        exporting: {
            enabled: false
        }
    });




    Highcharts.chart('teamWinYear', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Teams Wins Matches Per Year'
        },
        xAxis: {
            categories: Object.keys(data["matchesPerYear"]),
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total matches win'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        credits: {
            enabled: false
        },

        series: teamWonMatchYear(data['matchesWonTeamYear']),
        exporting: {
            enabled: false
        }
    });





    Highcharts.chart('extraRunPerTeam', {
        chart: {
            type: 'column'
        },
        title: {
            text: "Extra Runs Per Team 2016"
        },

        xAxis: {

            categories: Object.keys(data["extraRunsTeam2016"]),

        },
        yAxis: {
            min: 50,
            title: {
                text: 'Extra runs'
            }
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Extra Runs',
            data: Object.values(data["extraRunsTeam2016"]),
            showInLegend: false,
        }],
        exporting: {
            enabled: false
        }
    });




    Highcharts.chart('ecnomicalBowlers', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Economical Bowlers 2015'
        },

        xAxis: {

            categories: ecnomicaBowlersName,

        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy'
            }
        },
        credits: {
            enabled: false
        },

        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Economy',
            data: ecnomicaBowlersEconomy,

            showInLegend: false,
        }],
        exporting: {
            enabled: false
        }
    });
}